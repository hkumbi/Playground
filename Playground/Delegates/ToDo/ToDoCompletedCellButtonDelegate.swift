//
//  ToDoCompletedCellButtonDelegate.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import Foundation


protocol ToDoCompletedCellButtonDelegate : AnyObject {
    
    func todoPressed(_ id : UUID)
}

//
//  ToDoListOptionsButtonDelegate.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import Foundation


protocol ToDoListOptionsButtonDelegate : AnyObject {
    
    func buttonPressed(for type : ToDoListOptionsButtonType)
}


enum ToDoListOptionsButtonType {
    case cancel, deleteAll, create
}

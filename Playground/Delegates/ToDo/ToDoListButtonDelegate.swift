//
//  ToDoListButtonDelegate.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import Foundation


protocol ToDoListButtonDelegate : AnyObject {
    
    func buttonPressed(for type : ToDoListButtonType)
}


enum ToDoListButtonType {
    case completed, options
}

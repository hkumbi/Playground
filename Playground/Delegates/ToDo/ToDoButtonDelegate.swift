//
//  ToDoButtonDelegate.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import Foundation

protocol ToDoButtonDelegate : AnyObject {
    func close()
}

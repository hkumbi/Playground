//
//  CreateToDoButtonDelegate.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import Foundation


protocol CreateToDoButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CreateToDoButtonType)
}

enum CreateToDoButtonType {
    case back, create
}

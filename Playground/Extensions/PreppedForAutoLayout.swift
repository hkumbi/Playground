//
//  PreppedForAutoLayout.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import Foundation
import UIKit

extension UIView {
    
    static func viewPreppedForAutoLayout() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}


extension UIButton {
    
    static func preppedForAutoLayout() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}


extension UILabel {
    
    static func preppedForAutoLayout() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}


extension UICollectionView {
    
    static func preppedForAutoLayout() -> UICollectionView {
        let collection = UICollectionView()
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }
}


extension UITableView {
    
    static func preppedForAutoLayout() -> UITableView {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }
}


extension UIImageView {
    
    static func preppedForAutoLayout() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}


extension UITextView {
    
    static func preppedForAutoLayout() -> UITextView {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }
}


extension UITextField {
    
    static func preppedForAutoLayout() -> UITextField {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
}


extension IconButton {
    
    static func iconPreppedForAutoLayout() -> IconButton {
        let iconButton = IconButton()
        iconButton.translatesAutoresizingMaskIntoConstraints = false
        return iconButton
    }
}


extension DefaultCollectionView {
    
    static func defaultPreppedForAutoLayout() -> DefaultCollectionView {
        let collection = DefaultCollectionView()
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }
}


extension DefaultTableView {
    
    static func defaultPreppedForAutoLayout(_ style : UITableView.Style = .plain) -> DefaultTableView {
        let tableView = DefaultTableView(style)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }
}


extension BackgroundButton {
    
    static func backgroundPreppedForAutoLayout() -> BackgroundButton {
        let button = BackgroundButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}


extension HeaderView {
    
    static func headerPreppedForAutoLayout() -> HeaderView {
        let header = HeaderView()
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }
}


extension BottomBorderTextField {
    
    static func borderPreppedForAutoLayout() -> BottomBorderTextField {
        let border = BottomBorderTextField()
        border.translatesAutoresizingMaskIntoConstraints = false
        return border
    }
}

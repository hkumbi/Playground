//
//  ScreenSize.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import Foundation
import UIKit

extension UIScreen {
    
    static var size : CGSize = .zero
    
    static var width : CGFloat {
        get { size.width }
    }
    
    static var height : CGFloat {
        get { size.height }
    }
    
}

//
//  TitleColor.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import Foundation
import UIKit


extension UIButton {
    
    func setTitleColorPress(_ color : UIColor) {
        setTitleColor(color, for: .normal)
        setTitleColor(color.withAlphaComponent(0.5), for: .highlighted)
        setTitleColor(color.withAlphaComponent(0.4), for: .disabled)
    }
}

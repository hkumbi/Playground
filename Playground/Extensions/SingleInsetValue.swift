//
//  SingleInsetValue.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import Foundation
import UIKit

extension UIEdgeInsets {
    
    static func from(_ value : CGFloat) -> UIEdgeInsets {
        UIEdgeInsets(top: value, left: value, bottom: value, right: value)
    }
    
}

//
//  StringSize.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import Foundation
import UIKit


extension String {
    
    static func getLabelHeight(text: String, font: UIFont, width: CGFloat = 0) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    static func getLabelWidth(text: String, font: UIFont, height: CGFloat = 0) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    
    static func getLabelHeightWithSpacing(label : UILabel, spacing : CGFloat, width : CGFloat) -> CGFloat {
        
        guard let text = label.text else { return .zero }
        
        let newLabel = UILabel()
        let font = label.font!
        let attributedString = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        
        newLabel.frame = CGRect(origin: .zero, size: CGSize(width: width, height: .greatestFiniteMagnitude))
        
        paragraphStyle.lineSpacing = spacing
        paragraphStyle.alignment = label.textAlignment
        
        attributedString.addAttribute(
            .paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length)
        )
        
        newLabel.attributedText = attributedString
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle], context: nil)
        
        return ceil(boundingBox.height)

    }
    
}

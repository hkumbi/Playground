//
//  ThemeColors.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-21.
//

import Foundation
import UIKit


extension UIColor {
    
    struct ThemeColors {
        static let purple = UIColor(red: 143/255, green: 57/255, blue: 133/255, alpha: 1)
    }
    
}

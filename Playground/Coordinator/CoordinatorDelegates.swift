//
//  CoordinatorDelegates.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-25.
//

import Foundation


protocol Coordinator : AnyObject {
    func start()
    func finished()
}

protocol ParentCoordinator : AnyObject {
    var childCoordinators : [Coordinator] { get }
    func childDidFinish(_ child : Coordinator)
}



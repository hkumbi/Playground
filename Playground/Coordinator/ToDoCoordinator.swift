//
//  ToDoCoordinator.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-24.
//

import Foundation


class ToDoCoordinator : Coordinator {
    
    private let navigation : MainNavigationController
    
    weak var parentCoordinator : ParentCoordinator?
    
    
    init(_ naviagation : MainNavigationController) {
        self.navigation = naviagation
    }
    
    func start() {
        let toDoListVC = ToDoListViewController()
        navigation.pushViewController(toDoListVC, animated: true)
    }
    
    func finished() {
        parentCoordinator?.childDidFinish(self)
    }
    
    
}

//
//  AppCoordinator.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-24.
//

import Foundation
import UIKit


class AppCoordinator : Coordinator, ParentCoordinator {
    
    private let navigation : MainNavigationController
    var childCoordinators = [Coordinator]()

    
    init(_ navigation : MainNavigationController) {
        self.navigation = navigation
    }
    
    
    func start() {
        let todoCoordinator = ToDoCoordinator(navigation)
        todoCoordinator.parentCoordinator = self
        childCoordinators.append(todoCoordinator)
        todoCoordinator.start()
    }
    
    func finished() {
        navigation.popToRootViewController(animated: true)
    }
    
    func childDidFinish(_ child: Coordinator) {
        for (i, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: i)
            }
        }
    }
    
}


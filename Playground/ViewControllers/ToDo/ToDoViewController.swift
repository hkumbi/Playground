//
//  ToDoViewController.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import UIKit


protocol ToDoCoordinatorDelegate : AnyObject {
    func closeEventDidFire(_ vc : ToDoViewController)
}

class ToDoViewController: UIViewController {
    
    private let contentView = ToDoView()
    
    private var closeHeight : CGFloat = 0
    
    private var modalHeight : CGFloat {
        get { contentView.modalView.height }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.modalView.title = "THis is a title for todo"
        contentView.modalView.todoText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
        
        setUpPanGesture()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.modalView.bottomSafeAreaConstant = view.safeAreaInsets.bottom
        closeHeight = contentView.modalView.height/4
        contentView.modalBottomConstant = modalHeight
        animateOpen()

    }
    
    private func setUpPanGesture() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        contentView.modalView.addGestureRecognizer(pan)
    }
    
    @objc private func handlePanGesture(_ pan : UIPanGestureRecognizer) {
        
        let transY = pan.translation(in: view).y
        let veloY = pan.velocity(in: view).y
        
        switch pan.state {
        case .changed:
            if transY >= 0 {
                let multiplier = 1-(transY/modalHeight)
                contentView.modalBottomConstant = transY
                contentView.dimmingAlpha = ToDoView.maxDimmingAlpha*multiplier
            }
            
        case .ended:
            if closeHeight <= transY {
                animateClose()
            } else {
                animateOpen()
            }
            
        default:
            break
        }
    }
    

}

extension ToDoViewController : ToDoButtonDelegate{
    
    func close() {
        animateClose()
    }
    
}


extension ToDoViewController {
    
    private func animateOpen() {
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in

            contentView.modalBottomConstant = 0
            contentView.dimmingAlpha = ToDoView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = modalHeight
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            dismiss(animated: false)
        }
    }
    
}

//
//  CreateToDoViewController.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import UIKit
import CoreData


enum CreateToDoVCCoordinatorEvents {
    case backPressed, createToDo
}

protocol CreateToDoCoordinatorDelegate : AnyObject {
    func eventDidFire(for type : CreateToDoVCCoordinatorEvents, _ vc : CreateToDoViewController)
}

class CreateToDoViewController: UIViewController {
    
    private let contentView = CreateToDoView()
    private let validTextSet = CharacterSet.alphanumerics.union(.alphanumerics).union(.whitespaces).union(.punctuationCharacters)
    
    private let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    private var managedObjectContext : NSManagedObjectContext {
        get { appDelegate.persistentContainer.viewContext }
    }
    
    private var titleIsValid : Bool = false {
        didSet { contentView.createIsEnabled = taskIsValid && titleIsValid }
    }
    
    private var taskIsValid : Bool = false {
        didSet { contentView.createIsEnabled = taskIsValid && titleIsValid }
    }
    
    var todoTitle : String {
        get { contentView.title }
    }
    
    var todoTask : String {
        get { contentView.task }
    }
    
    unowned var coordinatorDelegate : CreateToDoCoordinatorDelegate!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.textViewDelegate = self
        contentView.textFieldDelegate = self
        
        
    }
    
}


extension CreateToDoViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let maxCharacterReached = newText.count > 300
        let textIsValid = newText.trimmingCharacters(in: validTextSet).isEmpty
        
        titleIsValid = !maxCharacterReached && textIsValid && !newText.isEmpty
        
        if titleIsValid || newText.isEmpty {
            contentView.titleError = ""
        } else if !textIsValid {
            contentView.titleError = "Only letters, numbers, punctuation, and spaces are allowed"
        }else if maxCharacterReached {
            contentView.titleError = "Max Character Reached"
        }
        
        return true
    }
    
}


extension CreateToDoViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let maxCharacterReached = newText.count > 80
        let textIsValid = newText.trimmingCharacters(in: validTextSet).isEmpty
        
        taskIsValid = !maxCharacterReached && textIsValid && !newText.isEmpty
        
        if taskIsValid || newText.isEmpty {
            contentView.taskError = ""
        } else if !textIsValid {
            contentView.taskError = "Only letters, numbers, punctuation, and spaces are allowed"
        }else if maxCharacterReached {
            contentView.taskError = "Max Character Reached"
        }
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        contentView.taskPlaceholderIsHidden = !textView.text.isEmpty
    }
    
}


extension CreateToDoViewController : CreateToDoButtonDelegate {
    
    func buttonPressed(for type: CreateToDoButtonType) {
        
        switch type {
        case .back:
            print("Back")
        case .create:
            createToDo()
        }
    }
    
}

extension CreateToDoViewController {
    
    private func createToDo() {
        
        let newTodo = ToDo(context: managedObjectContext)
        newTodo.title = contentView.task
        newTodo.task = contentView.task
        
        guard managedObjectContext.hasChanges else { return }
        
        do {
            try managedObjectContext.save()
        } catch _ {
            showError(
                "Unable to create",
                "Playground was unable to create a new todo. Please try again later."
            )
            
            return
        }
        
        coordinatorDelegate.eventDidFire(for: .createToDo, self)
    }
    
    private func showError(_ title : String, _ message : String) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: .cancel))
        present(alertVC, animated: true)
    }
}

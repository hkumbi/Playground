//
//  ToDoListViewController.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import UIKit
import CoreData


enum ToDoListVCCoordinatorEvents {
    case completedPressed, optionsPressed, todoPressed
}

protocol ToDoListVCCoordinatorDelegate : AnyObject {
    func eventDidFire(for type : ToDoListVCCoordinatorEvents, _ vc : ToDoListViewController)
}

class ToDoListViewController: UIViewController {
    
    private typealias DataSource = UITableViewDiffableDataSource<SectionModel, ToDoModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, ToDoModel>
    
    private let contentView = ToDoListView()
    private lazy var dataSource = makeDataSource()
    
    private var managedObjectContext : NSManagedObjectContext {
        get { (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        setUpTableView()
        makeData()
        
        navigationItem.rightBarButtonItem
        navigationItem.leftBarButtonItem
        navigationItem.title = ""
        
        let request = ToDo.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        request.predicate = NSPredicate(format: "completed == NO")
        request.shouldRefreshRefetchedObjects = true
        request.returnsObjectsAsFaults = false
        let controller = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        controller.delegate = self

        try? controller.performFetch()
        
    }
    
    private func setUpTableView() {
        
        let tableView = contentView.tableView
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.rowHeight = ToDoTableViewCell.height
        tableView.allowsSelection = false
        tableView.register(ToDoTableViewCell.self, forCellReuseIdentifier: ToDoTableViewCell.id)
    }
    
    
    private func makeDataSource() -> DataSource {
        
        let source = DataSource(tableView: contentView.tableView){
            [unowned self]tableView, indexPath, item in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ToDoTableViewCell.id, for: indexPath)
            
            return cell
        }
        
        return source
    }
    
    private func makeData() {
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems([ToDoModel(), ToDoModel(), ToDoModel(), ToDoModel()])
        dataSource.apply(snap)
    }
}

extension ToDoListViewController : NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeContentWith snapshot: NSDiffableDataSourceSnapshotReference) {
        
        dataSource.apply(snapshot as NSDiffableDataSourceSnapshot, animatingDifferences: true)
    }
    
}


extension ToDoListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let completedAction = UIContextualAction(style: .normal, title: nil){
            _,_,completed in
            
            print("Completed")
            completed(true)
        }
        
        let deleteAction = UIContextualAction(style: .normal, title: nil){
            _,_,completed in
            
            print("Delete")
            completed(true)
        }
        
        let config = UISwipeActionsConfiguration(actions: [completedAction, deleteAction])
        
        config.performsFirstActionWithFullSwipe = false
        
        completedAction.image = UIImage(systemName: "checkmark.circle")
        completedAction.backgroundColor = .systemBlue
        
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .red
        
        return config
    }
    
}


extension ToDoListViewController : ToDoListButtonDelegate {
    
    func buttonPressed(for type: ToDoListButtonType) {
        
        switch type {
        case .completed:
            print("Completed")
            
        case .options:
            let optionsVC = ToDoListOptionsViewController()
            optionsVC.modalPresentationStyle = .overCurrentContext
            present(optionsVC, animated: false)
        }
    }
    
}


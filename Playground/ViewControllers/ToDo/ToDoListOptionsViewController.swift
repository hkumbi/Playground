//
//  ToDoListOptionsViewController.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import UIKit


enum ToDoListOptionsVCCoordinatorEvents {
    case create, deleteAll, close
}

protocol ToDoListOptionsVCCoordinatorDelegate : AnyObject {
    func eventDidFire(for type : ToDoListOptionsVCCoordinatorEvents, _ vc : ToDoListOptionsViewController)
}

class ToDoListOptionsViewController: UIViewController {
    
    private let contentView = ToDoListOptionsView()
    
    private var closeHeight : CGFloat = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        closeHeight = contentView.modalView.height*0.25
        contentView.modalView.bottomSafeArea = view.safeAreaInsets.bottom
        contentView.modalBottomConstant = contentView.modalView.height
        animateOpen()
    }
    
}


extension ToDoListOptionsViewController : ToDoListOptionsButtonDelegate {
    
    func buttonPressed(for type: ToDoListOptionsButtonType) {
        
        switch type {
        case .cancel:
            animateClose()
            
        case .deleteAll:
            print("Delete All")
        case .create:
            print("Create")
        }
    }
    
}


extension ToDoListOptionsViewController {
    
    private func animateOpen() {
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = 0
            contentView.dimmingAlpha = ToDoListOptionsView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = contentView.modalView.height
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            dismiss(animated: true)
        }
    }
    
}

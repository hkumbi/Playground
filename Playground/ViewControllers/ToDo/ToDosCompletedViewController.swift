//
//  ToDosCompletedViewController.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-21.
//

import UIKit


enum ToDosCompletedVCCoordinatorEvents {
    case backPressed, todoPressed
}

protocol ToDosCompletedCoordinatorDelegate : AnyObject {
    func eventDidFire(for type : ToDosCompletedVCCoordinatorEvents, _ vc : ToDosCompletedViewController)
}

class ToDosCompletedViewController: UIViewController {
    
    private typealias DataSource = UITableViewDiffableDataSource<SectionModel, ToDoModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, ToDoModel>
    
    private let contentView = ToDosCompletedView()
    private lazy var dataSource = makeDataSoource()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        setUpTableView()
        makeData()
    }
    
    private func setUpTableView() {
        let table = contentView.tableView
        table.delegate = self
        table.dataSource = dataSource
        table.allowsSelection = false
        table.rowHeight = ToDoCompletedTableViewCell.height
        table.register(ToDoCompletedTableViewCell.self, forCellReuseIdentifier: ToDoCompletedTableViewCell.id)
    }

    private func makeDataSoource() -> DataSource {
        
        let source = DataSource(tableView: contentView.tableView){
            [unowned self]tableView, indexPath, item in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ToDoCompletedTableViewCell.id, for: indexPath) as! ToDoCompletedTableViewCell
            
            cell.cellID = item.id
            cell.buttonDelegate = self
            
            return cell
        }
        
        return source
    }
    
    private func makeData() {
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems([ToDoModel(), ToDoModel(), ToDoModel(), ToDoModel()])
        dataSource.apply(snap)
    }

}


extension ToDosCompletedViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .normal, title: nil){
            [unowned self]_,_,completed in
            
            completed(true)
            
            var snap = dataSource.snapshot()
            let item = dataSource.itemIdentifier(for: indexPath)!
            snap.deleteItems([item])
            dataSource.apply(snap)
        }
        
        let uncompletedAction = UIContextualAction(style: .normal, title: nil) {
            [unowned self]_,_,completed in
            
            
            completed(true)
        }
        
        let config = UISwipeActionsConfiguration(actions: [deleteAction, uncompletedAction])
        
        
        config.performsFirstActionWithFullSwipe = false
        
        uncompletedAction.image = UIImage(systemName: "text.badge.xmark")
        uncompletedAction.backgroundColor = .blue
        
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .red
        
        return config
    }
    
}


extension ToDosCompletedViewController : ToDoCompletedCellButtonDelegate {
    
    func todoPressed(_ id: UUID) {
        let todoVC = ToDoViewController()
        todoVC.modalPresentationStyle = .overCurrentContext
        present(todoVC, animated: false)
    }

}

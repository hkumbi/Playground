//
//  ToDo+CoreDataProperties.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-24.
//
//

import Foundation
import CoreData


extension ToDo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ToDo> {
        return NSFetchRequest<ToDo>(entityName: "ToDo")
    }

    @NSManaged public var completed: Bool
    @NSManaged public var title: String?
    @NSManaged public var task: String?

}

extension ToDo : Identifiable {

}

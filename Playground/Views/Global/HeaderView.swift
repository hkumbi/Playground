//
//  HeaderView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-21.
//

import UIKit

class HeaderView: UIView {
    
    init() {
        super.init(frame: .zero)
        
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        let headerHeight = UIScreen.height*0.07
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            heightAnchor.constraint(equalToConstant: headerHeight)
        ])
    }

}

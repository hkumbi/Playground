//
//  DefaultTableView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-21.
//

import UIKit

class DefaultTableView: UITableView {

    init(_ style : UITableView.Style = .plain) {
        super.init(frame: .zero, style: style)
        
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIButton {
            return true
        }
        
        return super.touchesShouldCancel(in: view)
    }
    
}

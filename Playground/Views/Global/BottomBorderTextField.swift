//
//  BottomBorderTextField.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import UIKit

class BottomBorderTextField: UITextField {
    
    override var font: UIFont? {
        didSet { updateHeight() }
    }
    
    private let bottomView : UIView = .viewPreppedForAutoLayout()
    
    private var heightConstraint : NSLayoutConstraint!
    private let spacing : CGFloat = 20
    
    var bottomBorderColor : UIColor {
        get { bottomView.backgroundColor ?? .clear }
        set { bottomView.backgroundColor = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(bottomView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        bottomView.backgroundColor = .black.withAlphaComponent(0.2)
    }
    
    private func makeConstraints() {
        
        heightConstraint = heightAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            heightConstraint,
            
            bottomView.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomView.heightAnchor.constraint(equalToConstant: 1),
            bottomView.widthAnchor.constraint(equalTo: widthAnchor),
            bottomView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    private func updateHeight() {
        
        guard let font = font, heightConstraint != nil else { return }
        
        heightConstraint.constant = String.getLabelHeight(text: "", font: font) + spacing
    }

}

//
//  BackgroundButton.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import UIKit

class BackgroundButton: UIButton {

    override var isHighlighted: Bool {
        didSet { backgroundColor = isHighlighted ? highlightedColor : baseColor }
    }
    
    override var isEnabled: Bool {
        didSet { backgroundColor = isEnabled ? baseColor : disabledColor }
    }
    
    var baseColor : UIColor = .white {
        didSet { backgroundColor = baseColor }
    }
    var highlightedColor : UIColor = .black.withAlphaComponent(0.1)
    var disabledColor : UIColor = .white

    
}

//
//  IconButton.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import Foundation
import UIKit


class IconButton: UIButton {
    
    override var isHighlighted: Bool {
        didSet { tintColor = isHighlighted ? highlightedColor : baseColor }
    }
    
    override var isEnabled: Bool {
        didSet { tintColor = isEnabled ? baseColor : disabledColor }
    }
    
    private var baseColor : UIColor = .black
    private var highlightedColor : UIColor = .black.withAlphaComponent(0.5)
    private var disabledColor : UIColor = .black.withAlphaComponent(0.4)
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    func setIconColor(_ color : UIColor) {
        baseColor = color
        highlightedColor = color.withAlphaComponent(0.5)
        disabledColor = color.withAlphaComponent(0.4)

        if isHighlighted {
            tintColor = highlightedColor
        } else if !isEnabled {
            tintColor = disabledColor
        } else {
            tintColor = baseColor
        }
    }
    
}

//
//  CreateToDoView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import UIKit


class CreateToDoView: UIView {
    
    private let headerView : HeaderView = .headerPreppedForAutoLayout()
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let titleTextField : BottomBorderTextField = .borderPreppedForAutoLayout()
    private let titleErrorLabel : UILabel = .preppedForAutoLayout()
    private let taskTextView : UITextView = .preppedForAutoLayout()
    private let taskPlaceholderLabel : UILabel = .preppedForAutoLayout()
    private let taskErrorLabel : UILabel = .preppedForAutoLayout()
    private let createButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : CreateToDoButtonDelegate!
    
    var textFieldDelegate : UITextFieldDelegate? {
        get { titleTextField.delegate }
        set { titleTextField.delegate = newValue }
    }
    
    var textViewDelegate : UITextViewDelegate? {
        get { taskTextView.delegate }
        set { taskTextView.delegate = newValue }
    }
    
    var taskPlaceholderIsHidden : Bool {
        get { taskPlaceholderLabel.isHidden }
        set { taskPlaceholderLabel.isHidden = newValue }
    }
    
    var titleError : String {
        get { titleErrorLabel.text ?? "" }
        set { titleErrorLabel.text = newValue }
    }
    
    var taskError : String {
        get { taskErrorLabel.text ?? "" }
        set { taskErrorLabel.text = newValue }
    }
    
    var createIsEnabled : Bool {
        get { createButton.isEnabled }
        set { createButton.isEnabled = newValue }
    }
    
    var title : String {
        get { titleTextField.text ?? "" }
    }
    
    var task : String {
        get  { taskTextView.text }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(headerView)
        self.addSubview(backButton)
        self.addSubview(titleLabel)
        self.addSubview(titleTextField)
        self.addSubview(titleErrorLabel)
        self.addSubview(taskTextView)
        self.addSubview(taskPlaceholderLabel)
        self.addSubview(taskErrorLabel)
        self.addSubview(createButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToVerticalSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        createButton.layer.cornerRadius = createButton.frame.height/2
    }

    private func configureViews() {
        print(UIFont.preferredFont(forTextStyle: .body).pointSize)
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.setIconColor(.black)
        backButton.pinImageView()
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        titleLabel.text = "Create ToDo"
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
        
        let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18))
        
        titleTextField.attributedPlaceholder = NSAttributedString(string: "Title", attributes: [
            NSAttributedString.Key.font: titleFont,
            NSAttributedString.Key.foregroundColor : UIColor.darkGray
        ])
        titleTextField.textColor = .black
        titleTextField.textAlignment = .center
        titleTextField.bottomBorderColor = .black.withAlphaComponent(0.4)
        titleTextField.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18))
        
        let errorFont = UIFont.systemFont(ofSize: 14)
        
        titleErrorLabel.textColor = .red
        titleErrorLabel.font = errorFont
        
        let taskFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        taskTextView.textColor = .black
        taskTextView.font = taskFont
        taskTextView.textContainerInset = .from(15)
        taskTextView.layer.cornerRadius = UIScreen.width*0.055
        taskTextView.layer.borderWidth = 1
        taskTextView.layer.borderColor = UIColor.black.withAlphaComponent(0.4).cgColor
        
        taskPlaceholderLabel.text = "Write your task . . ."
        taskPlaceholderLabel.textColor = .darkGray
        taskPlaceholderLabel.font = taskFont
        
        taskErrorLabel.textColor = .red
        taskErrorLabel.font = errorFont
        
        createButton.setTitle("Create", for: .normal)
        createButton.setTitleColor(.white, for: .normal)
        createButton.titleLabel?.font = UIFont.textButton
        createButton.baseColor = .ThemeColors.purple
        createButton.highlightedColor = .ThemeColors.purple.withAlphaComponent(0.5)
        createButton.disabledColor = .ThemeColors.purple.withAlphaComponent(0.4)
        createButton.isEnabled = false
        createButton.addTarget(self, action: #selector(createButtonPressed), for: .touchUpInside)
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(keyboardDismiss))
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        let topSpacing = UIScreen.height*0.06
        let smallTopSpacing = topSpacing*0.2
        let spacing = UIScreen.width*0.05
        
        NSLayoutConstraint.activate([
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            
            titleTextField.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: smallTopSpacing),
            titleTextField.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            titleTextField.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            titleErrorLabel.topAnchor.constraint(equalTo: titleTextField.bottomAnchor, constant: 5),
            titleErrorLabel.leftAnchor.constraint(equalTo: titleTextField.leftAnchor),
            
            taskTextView.topAnchor.constraint(equalTo: titleErrorLabel.bottomAnchor, constant: topSpacing),
            taskTextView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            taskTextView.heightAnchor.constraint(equalTo: taskTextView.widthAnchor, multiplier: 0.6),
            taskTextView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            taskPlaceholderLabel.topAnchor.constraint(equalTo: taskTextView.topAnchor, constant: 15),
            taskPlaceholderLabel.leftAnchor.constraint(equalTo: taskTextView.leftAnchor, constant: 15+5),
            
            taskErrorLabel.topAnchor.constraint(equalTo: taskTextView.bottomAnchor, constant: 5),
            taskErrorLabel.leftAnchor.constraint(equalTo: taskTextView.leftAnchor),
            
            createButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.95),
            createButton.heightAnchor.constraint(equalTo: createButton.widthAnchor, multiplier: 0.15),
            createButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            createButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
        ])
    }
    
    @objc private func keyboardDismiss() {
        endEditing(true)
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.buttonPressed(for: .back)
    }
    
    @objc private func createButtonPressed() {
        buttonDelegate.buttonPressed(for: .create)
    }
}

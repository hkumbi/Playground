//
//  ToDoModalView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import UIKit

class ToDoModalView: UIView {
    
    private let closeButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let todoLabel : UILabel = .preppedForAutoLayout()
    
    private let minimumHeight : CGFloat
    private let topSpacing = UIScreen.height*0.04
    private let buttonSize = UIScreen.width*0.07
    private var titleHeight : CGFloat = 0
    private var toDoHeight : CGFloat = 0
    private var toDoSpacing : CGFloat = 8
    private let titleWidth = UIScreen.width*0.8
    private let toDoWidth = UIScreen.width*0.9
    
    private var heightConstraint : NSLayoutConstraint!
    private let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
    private let descriptionFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    
    unowned var buttonDelegate : ToDoButtonDelegate!
    
    var title : String {
        get { titleLabel.text ?? "" }
        set { setTitleText(newValue) }
    }
    
    var todoText : String {
        get { todoLabel.text ?? "" }
        set { setToDoText(newValue) }
    }
    
    var height: CGFloat {
        get { heightConstraint.constant }
    }
    
    var bottomSafeAreaConstant : CGFloat = 0 {
        didSet { updateHeight() }
    }
    
    
    init() {
        
        minimumHeight = topSpacing*3 + buttonSize
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(closeButton)
        self.addSubview(titleLabel)
        self.addSubview(todoLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    

    private func configureViews() {
        backgroundColor = .white
        layer.cornerRadius = UIScreen.width*0.055
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        closeButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        closeButton.setIconColor(.black)
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.pinImageView()
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.font = titleFont
        
        todoLabel.font = descriptionFont
        todoLabel.textColor = .black
        todoLabel.numberOfLines = 0
    }
    
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        heightConstraint = heightAnchor.constraint(equalToConstant: minimumHeight)
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.width),
            heightConstraint,
                        
            closeButton.widthAnchor.constraint(equalToConstant: buttonSize),
            closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor),
            closeButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing/2),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            titleLabel.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: topSpacing/2),
            titleLabel.widthAnchor.constraint(equalToConstant: titleWidth),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            todoLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: topSpacing),
            todoLabel.widthAnchor.constraint(equalToConstant: toDoWidth),
            todoLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func closeButtonPressed() {
        buttonDelegate.close()
    }
    
}



extension ToDoModalView {
    
    private func setTitleText(_ text : String) {
        titleLabel.text = text
        titleHeight = String.getLabelHeight(text: text, font: titleFont, width: titleWidth)
        
        updateHeight()
    }
    
    private func setToDoText(_ text : String) {
        todoLabel.text = text
        todoLabel.addLineSpacing(toDoSpacing)
        toDoHeight = String.getLabelHeightWithSpacing(label: todoLabel, spacing: toDoSpacing, width: toDoWidth)

        updateHeight()
    }
    
    private func updateHeight() {
        heightConstraint.constant = titleHeight + toDoHeight + minimumHeight + bottomSafeAreaConstant
    }
    
}

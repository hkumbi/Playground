//
//  ToDoView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import UIKit

class ToDoView: UIView {
    
    static let maxDimmingAlpha : CGFloat = 0.6

    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    let modalView = ToDoModalView()
    
    private var modalBottomConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : ToDoButtonDelegate!
    
    var dimmingAlpha : CGFloat {
        get { dimmingView.alpha }
        set { dimmingView.alpha = newValue }
    }
    
    var modalBottomConstant : CGFloat {
        get { modalBottomConstraint.constant }
        set { modalBottomConstraint.constant = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(dimmingView)
        self.addSubview(modalView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pin(to: superview)
    }
    
    private func configureViews() {
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingTapped))
        tap.numberOfTapsRequired = 1
        dimmingView.addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        modalBottomConstraint = modalView.bottomAnchor.constraint(equalTo: bottomAnchor)
        dimmingView.pin(to: self)
        
        NSLayoutConstraint.activate([
            modalView.centerXAnchor.constraint(equalTo: centerXAnchor),
            modalBottomConstraint
        ])
    }

    @objc private func dimmingTapped() {
        buttonDelegate.close()
    }
}

//
//  ToDoListOptionsModalView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import UIKit

class ToDoListOptionsModalView: UIView {

    private let createButton = ToDoListOptionButton()
    private let deleteAllButton = ToDoListOptionButton()
    private let cancelButton : UIButton = .preppedForAutoLayout()
    
    private let contentHeight : CGFloat
    private let spacing = UIScreen.height*0.02
    
    private var heightConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : ToDoListOptionsButtonDelegate!
    
    var height : CGFloat {
        get { heightConstraint.constant }
    }
    
    var bottomSafeArea : CGFloat {
        get { heightConstraint.constant - contentHeight }
        set { heightConstraint.constant = newValue + contentHeight }
    }
    
    
    init() {
        
        let textHeight = String.getLabelHeight(text: "", font: UIFont.textButton)
        contentHeight = ToDoListOptionButton.height*2 + textHeight + spacing*3
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(createButton)
        self.addSubview(deleteAllButton)
        self.addSubview(cancelButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        layer.cornerRadius = UIScreen.width*0.055
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        createButton.icon = UIImage(systemName: "square.and.pencil")
        createButton.name = "Create ToDo"
        createButton.addTarget(self, action: #selector(createButtonPressed), for: .touchUpInside)
        
        deleteAllButton.icon = UIImage(systemName: "trash")
        deleteAllButton.name = "Delete All ToDos"
        deleteAllButton.addTarget(self, action: #selector(deleteAllButtonPressed), for: .touchUpInside)
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColorPress(.black)
        cancelButton.titleLabel?.font = UIFont.textButton
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        heightConstraint = heightAnchor.constraint(equalToConstant: contentHeight)
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.width),
            heightConstraint,
            
            createButton.topAnchor.constraint(equalTo: topAnchor, constant: spacing),
            createButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            deleteAllButton.topAnchor.constraint(equalTo: createButton.bottomAnchor),
            deleteAllButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            cancelButton.topAnchor.constraint(equalTo: deleteAllButton.bottomAnchor, constant: spacing),
            cancelButton.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func createButtonPressed() {
        buttonDelegate.buttonPressed(for: .create)
    }
    
    @objc private func deleteAllButtonPressed() {
        buttonDelegate.buttonPressed(for: .deleteAll)
    }
    
    @objc private func cancelButtonPressed() {
        buttonDelegate.buttonPressed(for: .cancel)
    }

}

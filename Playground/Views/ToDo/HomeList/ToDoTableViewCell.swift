//
//  ToDoTableViewCell.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {
    
    static let id = "ToDoTableViewCell"
    static let height : CGFloat = 100
    
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let todoDescriptionLabel : UILabel = .preppedForAutoLayout()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureViews()
        
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(todoDescriptionLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {

        titleLabel.text = "Hello this is a title"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        
        todoDescriptionLabel.text = "This is the descriptive text that will be shown"
        todoDescriptionLabel.textColor = .black
        todoDescriptionLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.width*0.07
        let textSpacing = Self.height*0.07
        
        
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: spacing),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -spacing),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -textSpacing),
            
            todoDescriptionLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: textSpacing),
            todoDescriptionLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: spacing),
            todoDescriptionLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -spacing),
        ])
    }
    
}

//
//  ToDoListView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import UIKit

class ToDoListView: UIView {

    private let headerView = HeaderView()
    private let completedButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let optionButton : IconButton = .iconPreppedForAutoLayout()
    let tableView : DefaultTableView = .defaultPreppedForAutoLayout()
    
    unowned var buttonDelegate : ToDoListButtonDelegate!
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(headerView)
        self.addSubview(completedButton)
        self.addSubview(titleLabel)
        self.addSubview(optionButton)
        self.addSubview(tableView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
        ])

    }
    
    private func configureViews() {
        
        backgroundColor = .white
        
        completedButton.setImage(UIImage(systemName: "checkmark.rectangle"), for: .normal)
        completedButton.setIconColor(.black)
        completedButton.pinImageView()
        completedButton.imageView?.contentMode = .scaleAspectFit
        completedButton.addTarget(self, action: #selector(completedButtonPressed), for: .touchUpInside)
        
        titleLabel.text = "To Dos"
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        
        optionButton.setImage(UIImage(systemName: "ellipsis"), for: .normal)
        optionButton.setIconColor(.black)
        optionButton.pinImageView()
        optionButton.imageView?.contentMode = .scaleAspectFit
        optionButton.addTarget(self, action: #selector(optionButtonPressed), for: .touchUpInside)
        
        tableView.separatorStyle = .singleLine
    }
    
    
    
    private func makeConstraints() {
        
        let spacing = UIScreen.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            completedButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            completedButton.heightAnchor.constraint(equalTo: completedButton.widthAnchor),
            completedButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            completedButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            titleLabel.leftAnchor.constraint(equalTo: completedButton.rightAnchor, constant: spacing),
            titleLabel.rightAnchor.constraint(equalTo: optionButton.leftAnchor, constant: -spacing),
            titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            
            optionButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            optionButton.heightAnchor.constraint(equalTo: optionButton.widthAnchor),
            optionButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            optionButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func completedButtonPressed() {
        buttonDelegate.buttonPressed(for: .completed)
    }
    
    @objc private func optionButtonPressed() {
        buttonDelegate.buttonPressed(for: .options)
    }
    
}

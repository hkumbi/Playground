//
//  ToDoListOptionButton.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-20.
//

import UIKit

class ToDoListOptionButton: BackgroundButton {
    
    static let height = UIScreen.height*0.08
    
    private let iconView : UIImageView = .preppedForAutoLayout()
    private let optionNameLabel : UILabel = .preppedForAutoLayout()
    
    var icon : UIImage? {
        get { iconView.image }
        set { iconView.image = newValue }
    }
    
    var name : String {
        get { optionNameLabel.text ?? "" }
        set { optionNameLabel.text = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(iconView)
        self.addSubview(optionNameLabel)
        
        makeConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        iconView.contentMode = .scaleAspectFit
        iconView.tintColor = .black

        optionNameLabel.textColor = .black
        optionNameLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.07
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.width),
            heightAnchor.constraint(equalToConstant: Self.height),
            
            iconView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.35),
            iconView.widthAnchor.constraint(equalTo: iconView.heightAnchor),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconView.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            optionNameLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: spacing),
            optionNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
}

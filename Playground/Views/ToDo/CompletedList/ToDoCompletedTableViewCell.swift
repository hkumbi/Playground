//
//  ToDoCompletedTableViewCell.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-22.
//

import UIKit

class ToDoCompletedTableViewCell: UITableViewCell {
    
    static let id = "ToDoCompletedTableViewCell"
    static let height : CGFloat = 100
    
    private let button : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let circleView : UIView = .viewPreppedForAutoLayout()
    private let checkmarkIconView : UIImageView = .preppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let todoDescriptionLabel : UILabel = .preppedForAutoLayout()
    
    var cellID : UUID!
    
    unowned var buttonDelegate : ToDoCompletedCellButtonDelegate!
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureViews()
        
        self.contentView.addSubview(button)
        self.contentView.addSubview(circleView)
        self.contentView.addSubview(checkmarkIconView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(todoDescriptionLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        button.baseColor = .white
        button.highlightedColor = .black.withAlphaComponent(0.05)
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        titleLabel.text = "Hello this is a title"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        
        todoDescriptionLabel.text = "This is the descriptive text that will be shown"
        todoDescriptionLabel.textColor = .black
        todoDescriptionLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))

        circleView.backgroundColor = .ThemeColors.purple
        
        checkmarkIconView.image = UIImage(systemName: "checkmark")
        checkmarkIconView.contentMode = .scaleAspectFit
        checkmarkIconView.tintColor = .white
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.width*0.07
        let textSpacing = Self.height*0.07
        let circleSize = Self.height*0.3
        
        circleView.layer.cornerRadius = circleSize/2
        
        button.pin(to: contentView)

        NSLayoutConstraint.activate([
            
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: spacing),
            titleLabel.rightAnchor.constraint(equalTo: circleView.leftAnchor, constant: -spacing),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -textSpacing),

            todoDescriptionLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: textSpacing),
            todoDescriptionLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            todoDescriptionLabel.rightAnchor.constraint(equalTo: circleView.leftAnchor, constant: -spacing),
            
            checkmarkIconView.widthAnchor.constraint(equalTo: circleView.widthAnchor, multiplier: 0.6),
            checkmarkIconView.heightAnchor.constraint(equalTo: checkmarkIconView.widthAnchor),
            checkmarkIconView.centerXAnchor.constraint(equalTo: circleView.centerXAnchor),
            checkmarkIconView.centerYAnchor.constraint(equalTo: circleView.centerYAnchor),
            
            circleView.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            circleView.heightAnchor.constraint(equalToConstant: circleSize),
            circleView.widthAnchor.constraint(equalTo: circleView.heightAnchor),
            circleView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
    
    @objc private func buttonPressed() {
        buttonDelegate.todoPressed(cellID)
    }
    
}

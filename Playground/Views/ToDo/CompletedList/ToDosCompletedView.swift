//
//  ToDosCompletedView.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-21.
//

import UIKit

class ToDosCompletedView: UIView {

    private let headerView = HeaderView()
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let clearButton : IconButton = .iconPreppedForAutoLayout()
    let tableView : DefaultTableView = .defaultPreppedForAutoLayout()
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(headerView)
        self.addSubview(backButton)
        self.addSubview(titleLabel)
        self.addSubview(clearButton)
        self.addSubview(tableView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
        ])
    }
    
    private func configureViews() {
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.setIconColor(.black)
        backButton.pinImageView()
        backButton.imageView?.contentMode = .scaleAspectFit

        titleLabel.text = "Completed ToDos"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        
        clearButton.setImage(UIImage(systemName: "trash"), for: .normal)
        clearButton.setIconColor(.red)
        clearButton.pinImageView()
        clearButton.imageView?.contentMode = .scaleAspectFit
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            clearButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            clearButton.heightAnchor.constraint(equalTo: clearButton.widthAnchor),
            clearButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            clearButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),

            tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
}

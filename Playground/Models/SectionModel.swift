//
//  SectionModel.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-21.
//

import Foundation


enum SectionModel : Hashable, Equatable {
    case main
}

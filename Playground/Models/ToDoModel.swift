//
//  ToDoModel.swift
//  Playground
//
//  Created by Herve Kumbi on 2022-09-21.
//

import Foundation


class ToDoModel: Hashable, Equatable {
    
    static func == (lhs: ToDoModel, rhs: ToDoModel) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
